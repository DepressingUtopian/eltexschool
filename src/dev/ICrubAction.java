package dev;

public interface ICrubAction {
    void create();
    void read();
    void update();
    void delete();
}
